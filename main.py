import sys
from huffman_tree import HuffmanTree
from file_handler import FileHandler

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: python main.py <encode/decode> <input_file> <output_file>")
        sys.exit(1)

    action, input_file, output_file = sys.argv[1], sys.argv[2], sys.argv[3]

    if action == 'encode':
        data = FileHandler.read_file(input_file)
        huffman_tree = HuffmanTree(data)
        encoding, the_tree = huffman_tree.huffman_encoding(data)
        FileHandler.save_data_to_file(encoding, the_tree, output_file)
    elif action == 'decode':
        encoding, the_tree = FileHandler.load_data_from_encoded_file(input_file)
        decoded_data = HuffmanTree.huffman_decoding(encoding, the_tree)
        with open(output_file, 'w') as file:
            file.write(decoded_data)
    else:
        print("Invalid command. Use 'encode' or 'decode'.")
