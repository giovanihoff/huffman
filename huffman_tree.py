from node import Node


# Baseado no código descrito em
# https://colab.research.google.com/drive/1XJzHlF1QB94uenN8cYxzKc0cL2WrIzcx
class HuffmanTree:
    def __init__(self, data):
        symbols_with_probs = self.calculate_probability(data)
        symbols = symbols_with_probs.keys()
        nodes = [Node(symbols_with_probs[symbol], symbol) for symbol in symbols]

        while len(nodes) > 1:
            nodes.sort(key=lambda x: x.probability)
            left = nodes.pop(0)
            right = nodes.pop(0)
            left.code = 0
            right.code = 1
            new_node = Node(left.probability + right.probability, left.symbol + right.symbol, left, right)
            nodes.append(new_node)

        self.root = nodes[0]

    @staticmethod
    def calculate_probability(data):
        symbols = {}
        for item in data:
            symbols[item] = symbols.get(item, 0) + 1
        return symbols

    def calculate_codes(self, node, value=''):
        codes = {}
        if node.left:
            codes.update(self.calculate_codes(node.left, value + '0'))
        if node.right:
            codes.update(self.calculate_codes(node.right, value + '1'))
        if not node.left and not node.right:
            codes[node.symbol] = value
        return codes

    def huffman_encoding(self, data):
        huffman_encoding = self.calculate_codes(self.root)
        print("Symbols with codes", huffman_encoding)
        self.total_gain(data, huffman_encoding)
        encoded_output = self.output_encoded(data, huffman_encoding)
        return encoded_output, self.root

    @staticmethod
    def output_encoded(data, coding):
        encoding_output = [coding[element] for element in data]
        return ''.join(encoding_output)

    @staticmethod
    def total_gain(data, coding):
        before_compression = len(data) * 8
        after_compression = sum(len(coding[symbol]) * data.count(symbol) for symbol in coding)
        print("Space usage before compression (in bits):", before_compression)
        print("Space usage after compression (in bits):", after_compression)

    @staticmethod
    def reconstruct_tree(data):
        if data is None:
            return None
        left_data = data['left']
        right_data = data['right']
        left = HuffmanTree.reconstruct_tree(left_data)
        right = HuffmanTree.reconstruct_tree(right_data)
        return Node(data['probability'], data['symbol'], left, right)

    @staticmethod
    def huffman_decoding(encoded_data, huffman_tree):
        tree_head = huffman_tree
        decoded_output = []
        for bit in encoded_data:
            if bit == '1':
                huffman_tree = huffman_tree.right
            elif bit == '0':
                huffman_tree = huffman_tree.left

            if not huffman_tree.left and not huffman_tree.right:
                decoded_output.append(huffman_tree.symbol)
                huffman_tree = tree_head

        return ''.join(decoded_output)
