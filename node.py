class Node:

    # Baseado no código descrito em
    # https://colab.research.google.com/drive/1XJzHlF1QB94uenN8cYxzKc0cL2WrIzcx
    def __init__(self, probability, symbol, left=None, right=None):
        self.probability = probability
        self.symbol = symbol
        self.left = left
        self.right = right
        self.code = ''

    # Necessário para formar o objeto durante a escrita no arquivo
    def to_dict(self):
        if self.left and self.right:
            return {
                'probability': self.probability,
                'symbol': self.symbol,
                'left': self.left.to_dict(),
                'right': self.right.to_dict()
            }
        else:
            return {
                'probability': self.probability,
                'symbol': self.symbol,
                'left': None,
                'right': None
            }
