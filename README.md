# Projeto

# Compactação de Arquivos com Codificação Huffman

```
CP-702 - Análise de Algoritmos e Complexidade
(TrabalhoemGrupodeatécincoalunos)
Prof. Paulo André Castro
```
1. Objetivo
    Exercitar e fixar conhecimentos adquiridos sobre: estruturas de dados, análise de algoritmos e complexidade.
2. Descrição do Trabalho
Implementar um programa em Python capaz de compactar arquivos texto utilizando o algoritmo conhecido como
codificação de Huffman (Huffman Encoding).. A codificação de Huffman é um método de compressão que usa as

probabilidades de ocorrência dos símbolos no conjunto de dados a ser comprimido para determinar códigos de
tamanho variável para cada símbolo. O método foi desenvolvido em 1952 por David A. Huffman e publicado no
artigo "A Method for the Construction of Minimum-Redundancy Codes".

O programa deve ser capaz de ler um arquivo texto cujo nome pode ser pedido por interface texto ou fornecido em
linha de comando, por exemplo: arquivo_texto1.txt. O programa então compacta-o utilizando codificação de Huffman
gerando um arquivo de saída com nome arquivo_texto1.txt.zip.
Exemplo:>> pythonencode.pyarquivo_texto1.txt
Arquivogerado:arquivo_texto1.txt.zip
Exemplo2:>>pythonencode.py
Qualonomedoarquivoacompactar?:arquivo_texto1.txt
Arquivogerado:arquivo_texto1.txt.zip

O programa deve também ser capaz de descompactar um arquivo compactado previamente. Você pode separar o
código em dois arquivos encode.py e decode.py ou usar apenas um arquivo de código com comandos. Por exemplo:
python huffman.py encode arquivo_texto1.txt; e python huffman.py decode arquivo_texto1.txt.zip.

```
Exemplo:>> pythondecode.pyarquivo_texto1.txt.zip
Arquivogerado:arquivo_texto1.decoded.txt
Exemplo2:>>pythondecode.py
Qualonomedoarquivoadescompactar?:arquivo_texto1.txt.zip
Arquivogerado:arquivo_texto1.decoded.txt.zip
```
Observação: lembre-se que você precisa fazer o arquivo codificado (.zip) conter toda a informação necessária a
decodificação!O que pode incluir a codificação de Huffman.

3. Material a ser Entregue e Prazo
Devem ser entregues um relatório e um notebook com o código
Entregar através do Google Classroom!
OBS: Não compacte os arquivos em um zip (ou qq outro formato), faça os uploads dos dois arquivos!
    A. Código em formato Notebook (ver detalhes abaixo)
Prazo de Entrega: 30/Abril/2024;

Item A: Código do Projeto em Formato Notebook e formatado como um Relatório

Estrutura do Notebook do Projeto(arquivo em formato ipynb)
Título: Compactação de Arquivos com Codificação Huffman
Equipe: Nomes do membros da Equipe

1. Descrição do código e Formatação do Arquivo
2. Apresentação de exemplos de compactação e descompactação de arquivos
3. Conclusões, Comentários e Sugestões
OBS:Use células ‘Markdown’ para especificar cada célula de código e use comentários para descrever o código
Python.

```
Bom Trabalho!
Prof. Paulo André Castro
```