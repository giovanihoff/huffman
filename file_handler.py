import json

from huffman_tree import HuffmanTree


class FileHandler:

    @staticmethod
    def read_file(file_name):
        with open(file_name, 'r') as file:
            content = file.read()
        return content

    @staticmethod
    def save_data_to_file(encoding, the_tree, file_name):
        data = {
            'encoding': encoding,
            'the_tree': the_tree.to_dict() if the_tree else None
        }
        with open(file_name, 'w') as file:
            json.dump(data, file)

    @staticmethod
    def load_data_from_encoded_file(file_name):
        with open(file_name, 'r') as file:
            data = json.load(file)
        encoding = data['encoding']
        the_tree_data = data['the_tree']
        the_tree = HuffmanTree.reconstruct_tree(the_tree_data)
        return encoding, the_tree
